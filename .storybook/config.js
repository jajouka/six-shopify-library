import Vue from 'vue'
import Vuex from 'vuex'
import 'storybook-chromatic';

import { configure, addDecorator } from '@storybook/vue'
import { action } from '@storybook/addon-actions'

// Styles
import '@/src/assets/scss/_storybook.scss'

import { addParameters } from '@storybook/vue';
import { themes } from '@storybook/theming';
import sixTheme from './sixTheme';


// Option defaults.
addParameters({
  options: {
    theme: sixTheme,
  },
});



addDecorator(() => ({
  template: '<v-app><story/></v-app>',
}));




