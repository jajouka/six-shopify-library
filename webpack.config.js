/*
 * Switch between production and development mode using
 * NODE_ENV environment variable. (Defaults to development)
 * Pass '--debug' to disable uglification during a
 * production build.
 *
 * Example:
 *  $ NODE_ENV=production ionic serve
 */

require('dotenv').config();

const webpack = require('webpack');

new webpack.DefinePlugin({
  'process.env.NODE_ENV': process.env.NODE_ENV,
  'process.env.SHOPIFY_ACCESS_TOKEN': '"' + process.env.SHOPIFY_ACCESS_TOKEN
        + '"',
});

switch (process.env.NODE_ENV) {
case 'production':
case 'prod':
  module.exports = require('./webpack.config.prod.js');
  break;
case 'staging':
case 'stage':
  module.exports = require('./webpack.config.stage.js');
  break;
case 'development':
case 'dev':
default:
  module.exports = require('./webpack.config.dev.js');
  break;
}
