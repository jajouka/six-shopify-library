import gql from 'graphql-tag';
import { checkout } from '../gql/schema/checkout';

export const CREATE_CHECKOUT = gql`
mutation checkoutCreate($input: CheckoutCreateInput!) {
  checkoutCreate(input: $input) {
    ${checkout}
    checkoutUserErrors {
      code
      field
      message
    }
  }
}`;
