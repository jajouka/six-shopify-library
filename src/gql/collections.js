import gql from 'graphql-tag';

export const COLLECTIONS = gql`query {
  collections(first: 16) {
    edges {
      node {
        id
        title
        handle
        image {
          id
          altText
          transformedSrc(maxWidth: 600, maxHeight: 600)
        }
      }
    }
  }
}`;
