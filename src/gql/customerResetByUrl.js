import gql from 'graphql-tag';
import { customer } from './schema/customer';

export const CUSTOMER_RESET_BY_URL = gql`
mutation customerResetByUrl($resetUrl: URL!, $password: String!) {
  customerResetByUrl(resetUrl: $resetUrl, password: $password) {
    ${customer}
    customerAccessToken {
      accessToken
      expiresAt
    }
    customerUserErrors {
      code
      field
      message
    }
  }
}`;
