import gql from 'graphql-tag';
import { customer } from './schema/customer';

export const SET_DEFAULT_ADDRESS = gql`
  mutation customerDefaultAddressUpdate($customerAccessToken: String!, $addressId: ID!) {
    customerDefaultAddressUpdate(customerAccessToken: $customerAccessToken, addressId: $addressId) {
      ${customer}
      customerUserErrors {
        code
        field
        message
      }
    }
  }`;
