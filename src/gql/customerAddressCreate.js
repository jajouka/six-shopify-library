import gql from 'graphql-tag';
import { customerAddress } from './schema/customerAddress';

export const CUSTOMER_ADDRESS_CREATE = gql`
mutation customerAddressCreate($customerAccessToken: String!, $address: MailingAddressInput!) {
  customerAddressCreate(customerAccessToken: $customerAccessToken, address: $address) {
    ${customerAddress}
    customerUserErrors {
      code
      field
      message
    }
  }
}`;
