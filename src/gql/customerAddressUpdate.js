import gql from 'graphql-tag';
import { customerAddress } from './schema/customerAddress';

export const CUSTOMER_ADDRESS_UPDATE = gql`
mutation customerAddressUpdate($customerAccessToken: String!, $id: ID!, $address: MailingAddressInput!) {
  customerAddressUpdate(customerAccessToken: $customerAccessToken, id: $id, address: $address) {
    ${customerAddress}
    customerUserErrors {
      code
      field
      message
    }
  }
}`;
