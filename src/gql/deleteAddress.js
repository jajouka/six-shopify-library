import gql from 'graphql-tag';

export const DELETE_ADDRESS = gql`
mutation customerAddressDelete($id: ID!, $customerAccessToken: String!) {
  customerAddressDelete(id: $id, customerAccessToken: $customerAccessToken) {
    deletedCustomerAddressId
    customerUserErrors {
      code
      field
      message
    }
  }
}`;
