import gql from 'graphql-tag';
import { checkout } from '../gql/schema/checkout';

export const UPDATE_LINE_ITEMS = gql`
mutation checkoutLineItemsReplace($lineItems: [CheckoutLineItemInput!]!, $checkoutId: ID!) {
  checkoutLineItemsReplace(lineItems: $lineItems, checkoutId: $checkoutId) {
    ${checkout}
    userErrors {
      code
      field
      message
    }
  }
}`;
