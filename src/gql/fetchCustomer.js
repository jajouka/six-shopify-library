import gql from 'graphql-tag';
import { customer } from './schema/customer2';

export const FETCH_CUSTOMER = gql`query customer($token: String!) {

  
  ${ customer }
  
}
`;
