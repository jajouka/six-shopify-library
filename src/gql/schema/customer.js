export const customer = `customer {
  id
  displayName
  firstName
  lastName
  email
  phone
  acceptsMarketing 
  defaultAddress {
    id
  }
  addresses(first:5) {
    edges {
      node {
        id
        firstName
        lastName
        company
        address1
        address2
        city
        zip
        country
      }
    }
  }
  orders(first:100) {
    edges {
      node {
        currencyCode
        email
        phone
        name
        processedAt
        subtotalPriceV2 {
          amount
          currencyCode
        }
        totalPriceV2 {
          amount
          currencyCode
        }
        shippingAddress {
          id
          name
          company
          address1
          address2
          city
          zip
          country
        }
        successfulFulfillments {
          trackingCompany
          trackingInfo {
            number
            url
          }
        }
        lineItems(first: 100) {
          edges {
            node {
              title
              quantity
              variant {
                title
                image {
                  transformedSrc
                }
                priceV2 {
                  amount
                  currencyCode
                }
              }
              discountAllocations {
                allocatedAmount {
                  amount
                  currencyCode
                }
              }
            }
          }
        }
      }
    }
  }
}`;
