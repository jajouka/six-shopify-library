export const checkout = `checkout {
  id
  webUrl,
  lineItems(first: 5) {
    edges {
      node {
        title
        quantity
        variant {
          id
          title
          availableForSale
          quantityAvailable
          presentmentPrices(first: 10) {
            edges {
              node {
                price {
                  amount
                  currencyCode
                }
              }
            }
          }
        }
      }
    }
  }
}`;
