export const customerAddress = `customerAddress {
  id
  firstName
  lastName
  company
  address1
  address2
  city
  zip
  country
}`;
