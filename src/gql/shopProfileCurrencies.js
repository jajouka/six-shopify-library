import gql from 'graphql-tag';

export const SHOP_PROFILE_CURRENCIES = gql` {
  shop {
    paymentSettings {
      enabledPresentmentCurrencies
    }
  }
}`;
