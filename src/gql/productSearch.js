import gql from 'graphql-tag';

export const PRODUCT_SEARCH = gql` {
  query {
    products(first: 10, query: $query) {
      edges {
        node {
          id
          title
          handle
        }
      }
    }
  }
}`;