import gql from 'graphql-tag';
import { customer } from './schema/customer';

export const CUSTOMER_CREATE = gql`
mutation customerCreate($input: CustomerCreateInput!) {
  customerCreate(input: $input) {
    ${customer}
    customerUserErrors {
      code
      field
      message
    }
  }
}`;
