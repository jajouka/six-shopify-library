import gql from 'graphql-tag';

export const PRODUCT_RECOMMENDATIONS = gql`query productRecommendations($productId: ID!) {
  productRecommendations(productId: $productId) {
    title
    availableForSale
    id
    handle
    images(first: 1) {
      edges {
        node {
          id
          altText
          transformedSrc(maxWidth: 300, maxHeight: 300)
        }
      }
    }
  }
}`;
