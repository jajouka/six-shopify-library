import PanelSizes from '../components/Organisms/PanelSizes/PanelSizes';
//import PanelSizesMobile from '../components/Organisms/PanelSizesMobile/PanelSizesMobile';

export default {
  components: {
    PanelSizes,
    //PanelSizesMobile
  },
  mixins: [
  ],
  methods: {
    showPanelSizes() {
      this.$showPanel({
        //component: this.mq === 'medium' ? PanelSizesMobile : PanelSizes,
        component: PanelSizes,
        cssClass: 'c-Panel--sizes',
        width: '50%',
        openOn: 'right',
      });
    }
  },
  computed:{
    mq(){
      window.onresize(()=>{
        console.log(this.$mq);
      });

      return this.$mq;

    }

  }
};
