import Vue from 'vue';
import { mapGetters } from 'vuex';

const Currency = {
  install(Vue, options) {
    Vue.mixin({
      computed: {
        ...mapGetters({
          currencies: 'shopify/currency/getCurrencies',
          activeCurrency: 'shopify/currency/activeCurrency'
        })
      }
    });
  }
};

Vue.use(Currency);
