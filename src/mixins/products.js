import Utils from '../mixins/utils';
import {EventBus} from '@/plugins/event-bus';
import { UPDATE_LINE_ITEMS } from '../gql/updateLineItems';

export default {
    mixins: [
      Utils,
    ],
    methods: {
        fetchPresentmentPrices(obj) {///@TODO!!!!!! bad title - split function
            // console.log('fetchPresentmentPrices store');
            // console.log(this.$store);
            let arr1 = [];
            obj.lineItems.forEach(lineItem => arr1.push({
                  'variantId': lineItem.variant.id,
                  'quantity': parseInt(lineItem.quantity)
              })
            );
            // send mutation here
            this.$apollo.mutate({
                mutation: UPDATE_LINE_ITEMS,
                variables: {
                    lineItems: arr1,
                    checkoutId: this.$store.getters['shopify/cart/getCheckoutID']
                },
            }).then((res) => {
                this.addToCartLabel = 'Add to bag';
                let checkoutObj = res.data.checkoutLineItemsReplace.checkout;
                console.log('222checkoutObj');
                console.log(checkoutObj);
                if (checkoutObj) {
                    // shopify/cart/setCheckoutObject
                    let preOrderItems = [];
                    let availableForSale, quantityAvailable;


                    //@TODO - pre-order logic is in too many places now, needs centralising
                    checkoutObj.lineItems.edges.forEach(function(lineItem, index) {
                        availableForSale = lineItem.node.variant.availableForSale;
                        quantityAvailable = lineItem.node.variant.quantityAvailable;

                        if(/*availableForSale && */quantityAvailable <= 0){
                            preOrderItems.push(lineItem.node);
                        }
                    });

                    console.log('products.js preOrderItems:');
                    console.log(preOrderItems);

                    console.log('obj');
                    console.log(obj);

                    this.$store.dispatch('shopify/cart/setCheckoutObject', obj);
                    // this.$store.dispatch('shopify/cart/setPreOrderItems', preOrderItems);

                    EventBus.$emit('updatingCart', false);

                }
            }).catch((e) => {
                console.log(e);
                EventBus.$emit('updatingCart', false);
            });
        },
        async updateVariantPrices(currency) {
            if (this.variants) {
                await this.variants.forEach(variant => {
                    let price = null;
                    let v = variant.presentmentPrices.find(p => p.currency === currency);
                    if(typeof (v) !== 'undefined' && Object.prototype.hasOwnProperty.call(v, 'price')){
                        price = v.price;
                    }
                    else{
                        price = "99999999999";//tmp for testing missing presentment prices
                    }
                    variant.price = price;
                });
            }
        },
        getSelectedVariant(title) {
            let self = this;
            let selVariant;
            if (this.variants && title.length) {
                // variable product
                this.variants.forEach(function(variant){
                    if(variant.title === title){
                        selVariant = variant;
                    }
                });
            }
            return selVariant;
        },
        getHexFromColorName(name){
            let hex = '#000000';//@TODO - make defaults global
            let colorCodes =  this.$root.$store.getters['shopify/colorCodes/colorCodes'];
            colorCodes = colorCodes.codes;
            let formattedName = name.replace(/-/g, ' ').toLowerCase().trim() ;
            if(typeof colorCodes !== 'undefined'){
                if(Object.keys(colorCodes).length > 0){
                    Object.entries(colorCodes).forEach(colorCode => {
                      let key = colorCode[0];
                      let value = colorCode[1];
                      if(value.name.replace(/-/g, ' ').toLowerCase().trim() !== ''){
                            let colorCodeName = value.name.replace(/-/g, ' ').toLowerCase().trim();
                            if(colorCodeName === formattedName){
                                hex = value.hex.toUpperCase();
                            }
                        }
                    });
                }
            }
            return hex;
        },
        getVariantTitle(options, selectedOptions) {
            return this.formatVariantTitleFromOptions(options, selectedOptions);
        },
        formatVariantTitleFromOptions(options, selectedOptions){
            let variantTitleMatch = '';
            let divider = ' / ';
            if(options){
                if(options.length > 0){
                    options.forEach(function(option){
                        selectedOptions.forEach(function(selOption){
                            if(option.name === selOption.name){
                                variantTitleMatch += selOption.value + divider;
                            }
                        });
                    });
                    return variantTitleMatch.substring(0, variantTitleMatch.length - divider.length);
                }
            }
            return '';
        },
        buildSelectedOptions(options, name, value){

            if(! options.some(option => option.name === name)){
                options.push({name: name, value: value});
            } else {
                let idx = options.findIndex(function(currentValue, index, arr){
                    return currentValue.name === name;
                }, name);
                options.splice(idx, 1);
                options.push({name: name, value: value});
            }
            return options;
        },
        getProductOptionComponentName(name) {
            //@TODO - fallback to c-product-option if not available?
            return `c-product-option-${name.replace(/_/g, '-').toLowerCase()}`;
        },
        scrollToTop(){
            window.scroll({
                top: 0,
                left: 0,
                behavior: 'smooth'
            });
        },
        filterEmpties(selOpts){
              return selOpts.filter(option => typeof(option.value) !== 'undefined' && option.value);
          },
        buildVariantImagesFromOption(images, possibleAltTags, selOpts, triggerOpts){
            let self = this;
            let variantImages = [];
            let selOptsFiltered = this.getSelOptsWithTrigger(selOpts, triggerOpts);

          selOptsFiltered = this.filterEmpties(selOptsFiltered, triggerOpts);

          if(selOptsFiltered.length > 0){
              if(typeof (images) !== 'undefined'){
                  if(images.length > 0) {
                      images.forEach(function(img){
                          if(Object.prototype.hasOwnProperty.call(img, 'altL')){
                              if(typeof img.altL !== 'undefined'){

                                  if(typeof img.altL === 'string'){
                                      if(img.altL.trim() !== ''){
                                          let imgTags = img.altL.substring(0, img.altL.indexOf(']'));
                                          imgTags = imgTags.substr(1);
                                          let altMatchStr = self.getAltMatchStrFromSelOpts(selOptsFiltered);
                                          if(imgTags === altMatchStr){
                                              variantImages.push(img);
                                          }
                                      }
                                  }
                              }
                          }
                      });
                      return variantImages;
                  }
              }
          }

          return [];
        },
        getSelOptsWithTrigger(selOpts, triggerOpts){
            let filteredOpts = [];
            triggerOpts.forEach(function(triggerOpt){
                selOpts.forEach(function(opt){
                    if (triggerOpt.toLowerCase() === opt.name.toLowerCase()){
                        filteredOpts.push(opt);
                    }
                });
            });
            return filteredOpts;
        },
        getAltMatchStrFromSelOpts(selOpts){
            let self = this;
            let str = '';
            if(selOpts){
                selOpts.forEach(function(selOpt){
                    str += self.convertTagToStr(selOpt)
                });
            }
            str = str.substr(0, str.length - 1);
            return str;
        },
        convertTagToStr(tag){
            return tag.name.toLowerCase() + ':' + this.slugify(tag.value) + ',';
        },
        getValidImgAltTags(imgTagStr, possibleOptAltTags){//@TODO - not used, why? should it be?
            let self = this;
            let tags = [];

            let tagsRaw = str.match(/\[.*?\]/g, '');

            if (tagsRaw) {

                let split = tagsRaw[0].replace(/[\[\]']+/g,'').trim().split(',');
                split.forEach(function(tag){
                    let keyVal = tag.trim().split(':');
                    let imgTagOptObj = {
                        key: keyVal[0],
                        value: keyVal[1]
                    };
                    let isEqual = self.checkOptIsValid(imgTagOptObj, self.observerDeepClean(possibleOptAltTags));
                    if(isEqual){
                        tags.push(imgTagOptObj);
                    }
                });
                return tags;

            }
        },
        checkOptIsValid(imgTagOptObj, possibleOptAltTags){
            let self = this;
            let isEqual = false;
            for(let possibleOptAltTag of possibleOptAltTags){
                isEqual = self.isEqual(imgTagOptObj, possibleOptAltTag);
                if(isEqual) {
                    return isEqual;
                }
            }
        },
        buildPossibleOptAltTags(triggerOpts, opts){
            let self = this;
            let matches = [];
            let divider = ':';

            if(opts){
                opts.forEach(function(opt){
                    triggerOpts.forEach(function(triggerOpt){
                        opt.values.forEach(function(val){
                            if(opt.name === triggerOpt){
                                let str = opt.name.toLowerCase() + divider + self.slugify(val);
                                let keyVal = str.split(':');
                                matches.push({key: keyVal[0], value: keyVal[1]});
                            }
                        });
                    });
                });
            }

            return matches;
        },
        observerDeepClean(obj) {
            let cleaned = [];
            obj.forEach(function(childObj){
                let cleanChildObj = Object.keys(childObj).reduce(
                  (res, e) => Object.assign(res, { [e]: childObj[e] }),
                  {}
                );
                cleaned.push(cleanChildObj);
            });
            return cleaned;
        },
        slugify(text){ //@TODO - move to utils or filters
            return text.toString().toLowerCase()
              .replace(/\s+/g, '-')
              .replace(/[^\w\-]+/g, '')
              .replace(/\-\-+/g, '-')
              .replace(/^-+/, '')
              .replace(/-+$/, '');
        },
        isEqual(value, other) {  //@TODO - move to utils
            let self = this;
            let type = Object.prototype.toString.call(value);
            if (type !== Object.prototype.toString.call(other)) return false;
            if (['[object Array]', '[object Object]'].indexOf(type) < 0) return false;
            let valueLen = type === '[object Array]' ? value.length : Object.keys(value).length;
            let otherLen = type === '[object Array]' ? other.length : Object.keys(other).length;
            if (valueLen !== otherLen) return false;

            let compare = function (item1, item2) {
                let itemType = Object.prototype.toString.call(item1);
                if (['[object Array]', '[object Object]'].indexOf(itemType) >= 0) {
                    if (!self.isEqual(item1, item2)) return false;
                }
                else {
                    if (itemType !== Object.prototype.toString.call(item2)) return false;
                    if (itemType === '[object Function]') {
                        if (item1.toString() !== item2.toString()) return false;
                    } else {
                        if (item1 !== item2) return false;
                    }
                }
            };
            if (type === '[object Array]') {
                for (let i = 0; i < valueLen; i++) {
                    if (compare(value[i], other[i]) === false) return false;
                }
            } else {
                for (let key in value) {
                    if (value.hasOwnProperty(key)) {
                        if (compare(value[key], other[key]) === false) return false;
                    }
                }
            }
            return true;
        },
        getOption(options, name){
            let opt = '';
            let opts = options;
            if(opts){
                opts.filter(function(option){
                    if(option.name === name){
                        opt = option;
                    }
                    if(option.key === name){
                        opt = option;
                    }
                });
            }
            return opt;
        },
        sortOptions(options){
            let vals = options;

            if(!isNaN(vals[0])) {
                return vals.sort();
            } else {
                let order = [
                    'xxxs',
                    'xxs',
                    'xs',
                    's',
                    'm',
                    'l',
                    'xl',
                    'xxl',
                    'xxxl'
                ];

                return vals.sort(function(a, b) {
                    return order.indexOf(a.toLowerCase()) - order.indexOf(b.toLowerCase());
                });
            }

            // return vals.sort().sort(function(a, b) {
            //     return order.indexOf(a.toLowerCase()) - order.indexOf(b.toLowerCase());
            // });
        },
        getMetaField(meta, name){
            if(meta && meta.metafields && meta.metafields.edges){
                let obj = meta.metafields.edges.find((d)=>{
                    return d.node.key===name;///@TODO!!!!! - name should be global
                });
                if(obj && obj.node){
                    let field = obj.node.value;
                    console.log('field:', field);
                    return field;
                }
            }
            return null;
        },
    }
};
