export default {
  methods: {
    sortByParam(arr, param, sortAsc = true) {
      arr.sort((a, b) =>
        (sortAsc ? (parseFloat(a[param]) > parseFloat(b[param])) : (parseFloat(a[param]) < parseFloat(b[param]))) ? 1 : -1
      );
    },
    sortByNewest(arr, param) {
      arr.sort((a, b) => new Date(b[param]) - new Date(a[param]));
    },
    passwordLengthInvalid(str) {
      if (str.length < 5 || str.length > 12)
        return true;
    },
    passwordMatch(str1, str2) {
      if (str1 === str2 && str1.length > 1)
        return true;
    },
    formatPrice(value) {//@TODO - make reusable by making regex a parameter
      let numDecPoints = 0;
      let val = (value/1).toFixed(numDecPoints).replace('.', ',');
      return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    },
  }
};
