import { CREATE_CHECKOUT } from '../gql/createCheckout';
import { EventBus } from '@/plugins/event-bus';
import Utils from '../mixins/utils';

export default {
  mixins:[Utils],
  methods: {
    hasCheckoutObj(){
      if(Object.prototype.hasOwnProperty.call(this.$store.state.shopify, 'cart')){
        if(typeof this.$store.state.shopify.cart !== 'undefined'){
          if(this.$store.state.shopify.cart) {
            if (Object.prototype.hasOwnProperty.call( this.$store.state.shopify.cart, 'checkoutObject')) {
              if (typeof this.$store.state.shopify.cart.checkoutObject !== 'undefined') {
                if (this.$store.state.shopify.cart.checkoutObject) {
                  if (Object.keys(this.$store.state.shopify.cart.checkoutObject).length > 0) {
                    return true;
                  }
                }
              }
            }
          }
        }
      }
      return false;
    },

    //@TODO - all code needs securing with safe conditions and defaults
    //@TODO - add cart btn labels as params

    addToCart(productVariantId) {

      let self = this;
      EventBus.$emit('updatingCart', true);

      //TODO - need to use $refs here? use reactive data instead?
      let productQuantity = this.$refs.productQuantity.value;

      if (productQuantity > 0 && typeof(productQuantity) !== 'undefined') {
        let lineItemsToAdd = [{
          variantId: productVariantId,
          quantity: parseInt(productQuantity),
          // customAttributes: [{
          //   key: 'Your Pre-Order Will Ship',
          //   value: 'MM/DD/YYYY'
          // }]
        }];

        if (!this.hasCheckoutObj()) {
          //console.log('no checkout object, so create a new one');
          this.$apollo.mutate({
            mutation: CREATE_CHECKOUT,
            variables: {
              input: {
                lineItems: [] // lineItemsToAdd
              }
            },
          }).then((res) => {
            let checkoutObj = res.data.checkoutCreate.checkout;
            if (checkoutObj) {
              self.$store.dispatch('shopify/cart/setCheckoutObject', checkoutObj).then(function () {
                self.addLineItem(lineItemsToAdd);
              });
            }
          }).catch((e) => {
            console.log(e);
          });
        } else {
          this.addLineItem(lineItemsToAdd);
        }
      }
    },
    addLineItem(arr) {
      console.log('addLineItem arr');
      console.log(arr);
      this.$shopify.checkout.addLineItems(this.$store.getters['shopify/cart/getCheckoutID'], arr).then(checkout => {
        //console.log(checkout);
        this.fetchPresentmentPrices(checkout);
      });
    },
    getBagQuantity(lineItems){
      let count = 0;
      lineItems.forEach((lineItem) => {
        let qty = lineItem['quantity'];
        count = count + qty;
      });
      return count;
    }
  }
};
