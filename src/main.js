import Vue from 'vue';
import App from './App.vue';
import Vuex from 'vuex';
import router from './router/index.js';

Vue.config.productionTip = false;
Vue.use(Vuex); //@TODO - is this needed??

/*new Vue({
  el: '#app',
  components: { App },
  template: '<App/>'
});*/

new Vue({ el: '#app', router, render: h => h(App) });
