// routes.js
import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../components/Pages/Home.vue';
import Products from '../components/Pages/Products.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/products',
    name: 'products',
    component: Products
  },

];

export default new VueRouter({routes});
