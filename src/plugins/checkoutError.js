
export default ({ app }, inject) => {
  inject('clearCheckoutOnError', param => {
    const checkoutId = app.store.getters['shopify/cart/getCheckoutID'];
    if(typeof checkoutId !== 'undefined') {
      try {
        app.$shopify.checkout.fetch(app.store.getters['shopify/cart/getCheckoutID']).then(checkout => {

          //console.log('clearCheckoutOnError()');
          //console.log(checkout);

          if(typeof checkout !== 'undefined'){
            if(checkout){
              if(Object.keys(checkout).length > 0){
                if(Object.prototype.hasOwnProperty.call(checkout, 'completedAt')){
                  if(typeof checkout['completedAt'] !== 'undefined') {
                    if (typeof checkout['completedAt'] === 'string') {
                      if (checkout.completedAt.length > 0) {
                        console.log('checkout cleared');
                        app.store.dispatch('shopify/cart/setCheckoutObject', {});
                      }
                    }
                  }
                }
              }
            }
          }
        });
      } catch (e) {
        console.log('!error');
        console.log(e);
        // console.log('xxxxxxx');
        //app.store.dispatch('shopify/cart/setCheckoutObject', {});
      }
    }
  });
};

